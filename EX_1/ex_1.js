function sapXep() {
  var a = document.getElementById("so-1").value * 1;
  var b = document.getElementById("so-2").value * 1;
  var c = document.getElementById("so-3").value * 1;
  var result;
  if (a >= b && b >= c) {
    result = `${c} , ${b} , ${a}`;
  } else if (a >= b && b <= c && a >= c) {
    result = `${b} , ${c} , ${a}`;
  } else if (a >= b && b <= c && a <= c) {
    result = `${b} , ${a} , ${c}`;
  } else if (a <= b && b >= c && a >= c) {
    result = `${c} , ${a} , ${b}`;
  } else if (a <= b && b >= c && a <= c) {
    result = `${a} , ${c} , ${b}`;
  } else {
    result = `${a} , ${b} , ${c}`;
  }
  document.getElementById(
    "result"
  ).innerHTML = `<h1 class="py-4"> Thứ tự tăng dần của 3 số là: ${result}</h1>`;
}

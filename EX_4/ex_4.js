function duDoan() {
  var a = document.getElementById("canh-1").value * 1;
  var b = document.getElementById("canh-2").value * 1;
  var c = document.getElementById("canh-3").value * 1;
  var result;
  if (a == b && b == c) {
    result = `tam giác đều`;
  } else if ((a == b && b != c) || (a == c && b != c) || (b == c && a != c)) {
    result = `tam giác cân`;
  } else if (
    a * a == b * b + c * c ||
    b * b == a * a + c * c ||
    c * c == a * a + b * b
  ) {
    result = `tam giác vuông`;
  } else {
    result = `tam giác khác`;
  }
  document.getElementById(
    "result"
  ).innerHTML = `<h1 class="py-4">Đây là hình ${result} </h1>`;
}

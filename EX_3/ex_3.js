function dem() {
  var so1 = document.getElementById("so-1").value * 1;
  var so2 = document.getElementById("so-2").value * 1;
  var so3 = document.getElementById("so-3").value * 1;
  var phanDu1 = so1 % 2;
  var phanDu2 = so2 % 2;
  var phanDu3 = so3 % 2;
  var tongPhanDu = phanDu1 + phanDu2 + phanDu3;
  var soChan = 0;
  var soLe = 0;
  if (tongPhanDu == 0) {
    soChan = 3;
  } else if (tongPhanDu == 1) {
    soChan = 2;
  } else if (tongPhanDu == 2) {
    soChan = 1;
  } else {
    soChan = 0;
  }
  soLe = 3 - soChan;
  document.getElementById(
    "result"
  ).innerHTML = `<h1 class="py-4"> Trong 3 số đã nhập có: ${soChan} số chẵn và ${soLe} số lẻ </h1>`;
}

function loiChao() {
  var thanhVien = document.getElementById("thanh-vien").value;
  var result;
  switch (thanhVien) {
    case "B": {
      result = `Bố`;
      break;
    }
    case "M": {
      result = `Mẹ`;
      break;
    }
    case "A": {
      result = `Anh trai`;
      break;
    }
    case "E": {
      result = `Em gái`;
      break;
    }
    default: {
      result = `Người lạ ơi`;
    }
  }
  document.getElementById(
    "result"
  ).innerHTML = `<h1 class="py-4">Xin chào ${result}</h1>`;
}
